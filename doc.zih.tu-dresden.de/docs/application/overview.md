# Application for Login and Resources

In order to use the HPC systems installed at ZIH, a project application form has to be filled in.
The HPC project manager should hold a professorship (university) or head a research group. You may
also apply for the "Schnupperaccount" (trial account) for one year. Check the Access page for
details.
