# Nutzungsbedingungen / Terms Of Use

**Attention:** Only the German version of the Terms of Use is binding.

These new Terms of Use are valid from April 1, 2018:
[HPC-Nutzungsbedingungen_20180305.pdf]**todo** %ATTACHURL%/HPC-Nutzungsbedingungen_20180305.pdf?t=1520317028

**The key points are are:** \* For support reasons, we store your contact data according to our IDM.
(Will be anonymized at least 15 months after the cancellation of the HPC login.) The data of the HPC
project (incl. contact of project leader) will be kept for further reference. \*Our HPC systems may
only be used according to the project description. \* Responsibilities for the project leader: \*
She has to assign a team member with an HPC login as the technical project administrator. She can do
this herself if she has a login at our systems. \* The project leader or the administrator will have
to add/remove members to their project. She has access to accounting data for her project. \* These
issues cover the data storage in our systems: \* Please work in th escratch file systems. \* Upon
request, the project leader or the administrator can be given access to a user's directory.  \* The
scratch file systems (/tmp, /scratch, /fastfs, /fasttemp) are for temporary use only. After a
certain time, files may be removed automatically (for /tmp after 7 days, parallel scratch: after 100
days).  \* Before a user leaves a project the leader/administrator has to store away worthy data.
For this, the storage services of ZIH (long term storage, intermediate archive) can be used. \*
Project termination (**new**) \* At project's end, jobs cannot be submitted and started any longer.
\* Logins are valid for 30 more days for saving data. \* Hundred days after project termination,
it's files will be deleted in the HPC file systems. \* The HPC user agrees to follow the
instructions and hints of the support team. In case of non-compliance, she can be disabled for the
batch system or banned from the system.

- Working with logs and HPC performance data (**new**)
  - For HPC related research ZIH will collect and analyze
    performance data. Anonymized, it might be shared with research partners.
  - Log data will be kept for long term analyzes.

These key points are only a brief summary. If in doubt, please consult the German original.

## Historie / History

| Valid                           | Document                                                                                              |
|:--------------------------------|:------------------------------------------------------------------------------------------------------|
| 1 April 2018 -                  | [HPC-Nutzungsbedingungen_20180305.pdf]**todo** %ATTACHURL%/HPC-Nutzungsbedingungen_20180305.pdf?t=1520317028 |
| 1 October 2016 - 31 March 2018  | [HPC-Nutzungsbedingungen_20160901.pdf]**todo** %ATTACHURL%/HPC-Nutzungsbedingungen_20160901.pdf              |
| 5 June 2014 - 30 September 2016 | [HPC-Nutzungsbedingungen_20140606.pdf]**todo** %ATTACHURL%/HPC-Nutzungsbedingungen_20140606.pdf              |

-   [Terms-of-use-HPC-20180305-engl.pdf]**todo** %ATTACHURL%/Terms-of-use-HPC-20180305-engl.pdf:
    Not binding translation in english
