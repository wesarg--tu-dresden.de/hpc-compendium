# Project Application for using the High Performance Computers

In order to use the HPC systems installed at ZIH, an application form has to be filled in. The
project applications are reviewed by the
[Scientific Advisory Board](https://tu-dresden.de/zih/die-einrichtung/wissenschaftlicher-beirat)
of the ZIH. The approval of the requested resources is always valid for one year. The project
duration for your project can span more than one year, but the resources will be granted for each
year individually.

**The HPC project manager should hold a professorship (university) or head a research group. The
project manager is called to inform the ZIH about any changes according the staff of the project
(retirements, a change to another institute).\<br /> (That is also regarding trial accounts. And
also trial accounts have to fill in the application form.)\<br />**

It is invariably possible to apply for more/different resources. Whether additional resources are
granted or not depends on the current allocations and on the availablility of the installed systems.

The terms of use of the HPC systems are only [available in German](terms_of_use.md) - at the
moment.

## Online Project Application

You may also apply for the "Schnupperaccount" (trial account) of up to 43.000 CPU hours for one year
(3500 CPUh per month). If you would like to continue after this, a **complete project application**
is required (see below).

For obtaining access to the machines, the following forms have to be filled in:

1. an [online application](https://hpcprojekte.zih.tu-dresden.de/) form for the project (one form
   per project).  The data will be stored automatically in a database.

1. Users/guests at TU Dresden without a ZIH-login have to fill in the following 
   [pdf] **todo** additionally.  TUD-external Users fill please  [this form (pdf)] **todo** 
   to get a login. Please sign
   and stamp it and send it by fax to +49 351 46342328, or by mail to TU Dresden, ZIH - Service
   Desk, 01062 Dresden, Germany. To add members with a valid ZIH-login to your
   project(s), please use this website: (https://hpcprojekte.zih.tu-dresden.de/managers/)
   (Access for proposers with valid ZIH-login after submission of the online application (see1.))

### Subsequent applications / view for project leader

Subsequent applications will be neccessary,

- if the end of project is reached
- if the applied resources won't be sufficient

The project leader and one person instructed by him, the project administrator, should use
[this website](https://hpcprojekte.zih.tu-dresden.de/managers/) (ZIH-login neccessary). At this
website you have an overview of your projects, the usage of resources, you can submit subsequent
applications, and you are able to add staff members to your project.

## Complete Project Application

Some reference points for an extended description of the application
form are:

- Presentation of the problem and description of project content (with
  references of publications)
- Description of achieved preliminary work, pre-studies with results,
  experiences
- Description of target objectives and target cognitions
- Description of physical and mathematical methods or solutions
- An idea of resources you need (time, number of CPUs, parallel use of
  CPUs)
- 1-2 figures are helpful to understand the description

### Here are some templates in German/English and Word/LaTeX.

Word-template(
[German](http://tu-dresden.de/die_tu_dresden/zentrale_einrichtungen/zih/dienste/formulare/projektantrag/dateien/zih-projektantrag-lang.doc),
[English](http://tu-dresden.de/die_tu_dresden/zentrale_einrichtungen/zih/dienste/formulare/projektantrag/dateien/zih-application-long.doc))
LaTeX-template(
[German](http://tu-dresden.de/die_tu_dresden/zentrale_einrichtungen/zih/dienste/formulare/projektantrag/dateien/zih-projektantrag-lang.tex),
[English](http://tu-dresden.de/die_tu_dresden/zentrale_einrichtungen/zih/dienste/formulare/projektantrag/dateien/zih-application-long.tex))

#### A small petition

If you plan to publish a paper with results based on the used CPU hours of our machines, please
insert in the acknowledgement an small part with thank for the support by the machines of the
ZIH/TUD. (see example below) Please send us a link/reference to the paper if it was puplished.  It
will be very helpfull for the next acquirement of compute power.  Thank you very much.

Two examples:

- The computations were performed on an Bull Cluster TAURUS at the
  Center for Information Services and High Performance Computing (ZIH)
  at TU Dresden.
- or We thank the Center for Information Services and High Performance
  Computing (ZIH) at TU Dresden for generous allocations of computer
  time.

## Access for Lectures/Courses

You would like to use the clusters at ZIH for your courses? No problem. Please initiate a normal
project application (see above), where you can tell us your wishes. In addition we need an
application for an HPC-login for the head of the course (also via link above).

Boundary conditions:

1. Teaching has uncomplicated access - but is subordinate to research projects
when resource problems occurs.
1. As with all other projects, access is only possible with
[online project application](https://tu-dresden.de/zih/hochleistungsrechnen/zugang/projektantrag).
1. For courses, there is no need to deliver additional detailed project description. (In exceptional
   cases, the ZIH reserves the right to require a more detailed abstract.)
1. 5.000 CPUh / month are possible without additional communication.
1. More than 5.000h / month should be possible after consulting the HPCSupport.
1. Post-iterations for 2.+3. are also possible at any time by arrangement.
1. For immediate job processing during classroom sessions (lecture, seminar) reservations are
   absolutely necessary. They have to be submitted at least 8 days before the appointment -
   via [mail to hpcsupport](mailto:hpcsupport@zih.tu-dresden.de).
