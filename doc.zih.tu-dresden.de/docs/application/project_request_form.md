# Project Request Form

## first step (requester)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="picture 2: personal information" width="170" zoom="on
">%ATTACHURL%/request_step1_b.png</span> <span class="twiki-macro IMAGE"
type="frame" align="right" caption="picture 1: login screen" width="170"
zoom="on
">%ATTACHURL%/request_step1_b.png</span>

The first step is asking for the personal informations of the requester.
**That's you**, not the leader of this project! \<br />If you have an
ZIH-Login, you can use it \<sup>\[Pic 1\]\</sup>. If not, you have to
fill in the whole informations \<sup>\[Pic.:2\]\</sup>. <span
class="twiki-macro IMAGE">clear</span>

## second step (project details)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="picture 3: project details" width="170" zoom="on
">%ATTACHURL%/request_step2_details.png</span> This Step is asking for
general project Details.\<br />Any project have:

-   a title, at least 20 characters long
-   a valid duration
    -   Projects starts at the first of a month and ends on the last day
        of a month. So you are not able to send on the second of a month
        a project request which start in this month.
    -   The approval is for a maximum of one year. Be carfull: a
        duratoin from "May, 2013" till "May 2014" has 13 month.
-   a selected science, according to the DFG:
    <http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/index.jsp>
-   a sponsorship
-   a kind of request
-   a project leader/manager
    -   The leader of this project should hold a professorship
        (university) or is the head of the research group.
    -   If you are this Person, leave this fields free.

<span class="twiki-macro IMAGE">clear</span>

## third step (hardware)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="picture 4: hardware" width="170" zoom="on
">%ATTACHURL%/request_step3_machines.png</span> This step inquire the
required hardware. You can find the specifications [here](../archive/hardware.md).
\<br />For your guidance:

-   gpu => taurus
-   many main memory => venus
-   other machines => you know it and don't need this guidance

<span class="twiki-macro IMAGE">clear</span>

## fourth step (software)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="picture 5: software" width="170" zoom="on
">%ATTACHURL%/request_step4_software.png</span> Any information you will
give us in this step, helps us to make a rough estimate, if you are able
to realize your project. For Example: you need matlab. Matlab is only
available on Taurus. <span class="twiki-macro IMAGE">clear</span>

## fifth step (project description)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="picture 6: project description" width="170" zoom="on
">%ATTACHURL%/request_step5_description.png</span> <span
class="twiki-macro IMAGE">clear</span>

## sixth step (summary)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="picture 8: summary" width="170" zoom="on
">%ATTACHURL%/request_step6.png</span> <span
class="twiki-macro IMAGE">clear</span>
