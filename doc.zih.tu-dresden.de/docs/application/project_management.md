# Project management

The HPC project leader has overall responsibility for the project and
for all activities within his project on ZIH's HPC systems. In
particular he shall:

-   add and remove users from the project,
-   update contact details of th eproject members,
-   monitor the resources his project,
-   inspect and store data of retiring users.

For this he can appoint a *project administrator* with an HPC account to
manage technical details.

The front-end to the HPC project database enables the project leader and
the project administrator to

-   add and remove users from the project,
-   define a technical administrator,
-   view statistics (resource consumption),
-   file a new HPC proposal,
-   file results of the HPC project.

## Access

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="password" width="100">%ATTACHURLPATH%/external_login.png</span>

[Entry point to the project management system](https://hpcprojekte.zih.tu-dresden.de/managers)

The project leaders of an ongoing project and their accredited admins
are allowed to login to the system. In general each of these persons
should possess a ZIH login at the Technical University of Dresden, with
which it is possible to log on the homepage. In some cases, it may
happen that a project leader of a foreign organization do not have a ZIH
login. For this purpose, it is possible to set a local password:
"[Passwort vergessen](https://hpcprojekte.zih.tu-dresden.de/managers/members/missingPassword)".

<span class="twiki-macro IMAGE" type="frame" align="right" caption="password reset"
width="100">%ATTACHURLPATH%/password.png</span>

On the 'Passwort vergessen' page, it is possible to reset the
passwords of a 'non-ZIH-login'. For this you write your login, which
usually corresponds to your email address, in the field and click on
'zurcksetzen'. Within 10 minutes the system sends a signed e-mail from
<hpcprojekte@zih.tu-dresden.de> to the registered e-mail address. this
e-mail contains a link to reset the password.

## Projects

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="projects overview"
width="100">%ATTACHURLPATH%/overview.png</span>

\<div style="text-align: justify;"> After login you reach an overview
that displays all available projects. In each of these projects are
listed, you are either project leader or an assigned project
administrator. From this list, you have the option to view the details
of a project or make a following project request. The latter is only
possible if a project has been approved and is active or was. In the
upper right area you will find a red button to log out from the system.
\</div> \<br style="clear: both;" /> \<br /> <span
class="twiki-macro IMAGE" type="frame" align="right"
caption="project details"
width="100">%ATTACHURLPATH%/project_details.png</span> \<div
style="text-align: justify;"> The project details provide information
about the requested and allocated resources. The other tabs show the
employee and the statistics about the project. \</div> \<br
style="clear: both;" />

### manage project members (dis-/enable)

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="project members" width="100">%ATTACHURLPATH%/members.png</span>
\<div style="text-align: justify;"> The project members can be managed
under the tab 'employee' in the project details. This page gives an
overview of all ZIH logins that are a member of a project and its
status. If a project member marked in green, it can work on all
authorized HPC machines when the project has been approved. If an
employee is marked in red, this can have several causes:

-   he was manually disabled by project managers, project administrator
    or an employee of the ZIH
-   he was disabled by the system because his ZIH login expired
-   his confirmation of the current hpc-terms is missing

You can specify a user as an administrator. This user can then access
the project managment system. Next, you can disable individual project
members. This disabling is only a "request of disabling" and has a time
delay of 5 minutes. An user can add or reactivate himself, with his
zih-login, to a project via the link on the end of the page. To prevent
misuse this link is valid for 2 weeks and will then be renewed
automatically. \</div> \<br style="clear: both;" />

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="add member" width="100">%ATTACHURLPATH%/add_member.png</span>

\<div style="text-align: justify;"> The link leads to a page where you
can sign in to a Project by accepting the term of use. You need also an
valid ZIH-Login. After this step it can take 1-1,5 h to transfer the
login to all cluster nodes. \</div> \<br style="clear: both;" />

### statistic

<span class="twiki-macro IMAGE" type="frame" align="right"
caption="project statistic" width="100">%ATTACHURLPATH%/stats.png</span>

\<div style="text-align: justify;"> The statistic is located under the
tab 'Statistik' in the project details. The data will updated once a day
an shows used CPU-time and used disk space of an project. Following
projects shows also the data of the predecessor. \</div>

\<br style="clear: both;" />
