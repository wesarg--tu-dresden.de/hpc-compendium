# PowerAI Documentation Links

There are different documentation sources for users to learn more about
the PowerAI Framework for Machine Learning. In the following the links
are valid for PowerAI version 1.5.4

## General Overview:

-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/en/SS5SF7_1.5.3/welcome/welcome.htm>"
    target="\_blank" title="Landing Page">Landing Page\</a> (note that
    you can select differnet PowerAI versions with the drop down menu
    "Change Product or version")
-   \<a
    href="<https://developer.ibm.com/linuxonpower/deep-learning-powerai/>"
    target="\_blank" title="PowerAI Developer Portal">PowerAI Developer
    Portal \</a>(Some Use Cases and examples)
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/en/SS5SF7_1.5.4/navigation/pai_software_pkgs.html>"
    target="\_blank" title="Included Software Packages">Included
    Software Packages\</a> (note that you can select different PowerAI
    versions with the drop down menu "Change Product or version")

## Specific User Howtos. Getting started with...:

-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted.htm>"
    target="\_blank" title="Getting Started with PowerAI">PowerAI\</a>
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_caffe.html>"
    target="\_blank" title="Caffe">Caffe\</a>
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_tensorflow.html?view=kc>"
    target="\_blank" title="Tensorflow">TensorFlow\</a>
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_tensorflow_prob.html?view=kc>"
    target="\_blank" title="Tensorflow Probability">TensorFlow
    Probability\</a>\<br />This release of PowerAI includes TensorFlow
    Probability. TensorFlow Probability is a library for probabilistic
    reasoning and statistical analysis in TensorFlow.
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_tensorboard.html?view=kc>"
    target="\_blank" title="Tensorboard">TensorBoard\</a>
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_snapml.html>"
    target="\_blank">Snap ML\</a>\<br />This release of PowerAI includes
    Snap Machine Learning (Snap ML). Snap ML is a library for training
    generalized linear models. It is being developed at IBM with the
    vision to remove training time as a bottleneck for machine learning
    applications. Snap ML supports many classical machine learning
    models and scales gracefully to data sets with billions of examples
    or features. It also offers distributed training, GPU acceleration,
    and supports sparse data structures.
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_pytorch.html>"
    target="\_blank">PyTorch\</a>\<br />This release of PowerAI includes
    the community development preview of PyTorch 1.0 (rc1). PowerAI's
    PyTorch includes support for IBM's Distributed Deep Learning (DDL)
    and Large Model Support (LMS).
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_caffe2ONNX.html>"
    target="\_blank">Caffe2 and ONNX\</a>\<br />This release of PowerAI
    includes a Technology Preview of Caffe2 and ONNX. Caffe2 is a
    companion to PyTorch. PyTorch is great for experimentation and rapid
    development, while Caffe2 is aimed at production environments. ONNX
    (Open Neural Network Exchange) provides support for moving models
    between those frameworks.
-   \<a
    href="<https://www.ibm.com/support/knowledgecenter/SS5SF7_1.5.4/navigation/pai_getstarted_ddl.html?view=kc>"
    target="\_blank" title="Distributed Deep Learning">Distributed Deep
    Learning\</a> (DDL). \<br />Works up to 4 TaurusML worker nodes.
    (Larger models with more nodes are possible with PowerAI Enterprise)

## PowerAI Container

We have converted the official Docker container to Singularity. Here is
a documentation about the Docker base container, including a table with
the individual software versions of the packages installed within the
container:

-   \<a href="<https://hub.docker.com/r/ibmcom/powerai/>"
    target="\_blank">PowerAI Docker Container Docu\</a>
