# Tests

Dies ist eine Seite zum Testen der Markdown-Syntax.

```python
import os

def debug(mystring):
  print("Debug: ", mystring)

debug("Dies ist ein Syntax-Highligthing-Test")
```
