# Access to the Cluster

## SSH access

Important note: ssh to Taurus is only possible from inside TU Dresden Campus. Users from outside
should use VPN
([see here](https://tu-dresden.de/zih/dienste/service-katalog/arbeitsumgebung/zugang_datennetz/vpn)).

The recommended way to connect to the HPC login servers directly via ssh:

```Bash
ssh <zih-login>@taurus.hrsk.tu-dresden.de
```

Please put this command in the terminal and replace `zih-login` with your login that you received
during the access procedure. Accept the host verifying and enter your password. You will be loaded
by login nodes in your Taurus home directory.  This method requires two conditions: Linux OS,
workstation within the campus network. For other options and details check the Login page.

Useful links: [Access]**todo link**, [Project Request Form](../application/request_for_resources.md),
[Terms Of Use]**todo link**
