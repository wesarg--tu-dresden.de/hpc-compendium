# What if everything didn't help?

## Create a Ticket: how do I do that?

The best way to ask about the help is to create a ticket. In order to do that you have to write a
message to the <a href="mailto:hpcsupport@zih.tu-dresden.de">hpcsupport@zih.tu-dresden.de</a> with a
detailed description of your problem. If possible please add logs, used environment and write a
minimal executable example for the purpose to recreate the error or issue.

## Communication with HPC Support

There is the HPC support team who is responsible for the support of HPC users and stable work of the
cluster. You could find the [details]**todo link** in the right part of any page of the compendium.
However, please, before the contact with the HPC support team check the documentation carefully
(starting points: [main page]**todo link**, [HPC-DA]**todo link**), use a search and then create a
ticket. The ticket is a preferred way to solve the issue, but in some terminable cases, you can call
to ask for help.

Useful link: [Further Documentation]**todo link**
