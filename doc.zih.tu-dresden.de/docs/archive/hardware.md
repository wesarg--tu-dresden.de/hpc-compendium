# Hardware

Here, you can find basic information about the hardware installed at ZIH. We try to keep this list
up-to-date.

- [BULL HPC-Cluster Taurus](taurus_ii.md)
- [SGI Ultraviolet (UV)](hardware_venus.md)

Hardware hosted by ZIH:

Former systems

- [PC-Farm Deimos](hardware_deimos.md)
- [SGI Altix](hardware_altix.md)
- [PC-Farm Atlas](hardware_atlas.md)
- [PC-Cluster Triton](hardware_triton.md)
- [HPC-Windows-Cluster Titan](hardware_titan.md)
