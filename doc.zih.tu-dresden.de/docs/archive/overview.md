# Archive

A warm welcome to the *archive*. You probably got here by following a link from within the compendium.
The archive holds outdated documentation for future reference. Documentation in the archive, is not
further updated.
