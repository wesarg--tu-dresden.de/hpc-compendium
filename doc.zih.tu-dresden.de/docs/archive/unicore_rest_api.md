# UNICORE access via REST API

**%RED%The UNICORE support has been abandoned and so this way of access
is no longer available.%ENDCOLOR%**

Most of the UNICORE features are also available using its REST API.

This API is documented here:

<https://sourceforge.net/p/unicore/wiki/REST_API/>

Some useful examples of job submission via REST are available at:

<https://sourceforge.net/p/unicore/wiki/REST_API_Examples/>

The base address for the Taurus system at the ZIH is:

unicore.zih.tu-dresden.de:8080/TAURUS/rest/core
